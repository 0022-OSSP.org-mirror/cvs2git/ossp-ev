#!/sw/bin/perl

@F = @ARGV;
@I = ();
$src = '';
foreach $f (@F) {
    open(FP, "<$f") || die;
    $file = '';
    $file .= $_ while (<FP>); 
    close(FP);
    if ($f =~ m|\.c$|) {
        $file =~ s|^.*?\*/\s*\n+||s;
        $file =~ s|^#include\s+(\S+)\s*\n+|push(@I, $1), ''|emg;
    }
    if ($f =~ m|\.h$|) {
        $file =~ s|^.*?\*/\s*\n+||s;
        $file =~ s|^#include\s+(\S+)\s*\n+|push(@I, $1), ''|emg;
    }
    $src .= "\n" .
            "/*\n" .
            "**  ==== ORIGINAL SOURCE: $f ====\n" .
            "*/\n" .
            "\n";
    $src .= $file;
}
$hdr = <<EOT;
/*
 * Copyright (c) 1995-1999 by Internet Software Consortium
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 * CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */
EOT
$inc1 = '';
$inc2 = '';
%brain = ();
foreach $i (@I) {
    if ($brain{$i} eq '') {
        $brain{$i} = 1;
        if ($i =~ m|^<|) {
            $inc1 .= "#include $i\n";
        }
        else {
            $inc2 .= "#include $i\n";
        }
    }
}
if ("@F" =~ m|\.c\s+|) {
    $inc2 = '#include "ev_lib.h"'."\n".
            '#include "ev_cfg.h"'."\n";
}
else {
    $inc2 = '';
}
$src = $hdr . "\n" . $inc1 . "\n" . $inc2 . "\n" . $src;

print $src;

