/*
**  ev_test.c -- Small test program for eventlib
**  Copyright (c) 1999 Ralf S. Engelschall, All Rights Reserved. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "ev.h"

struct myenv {
    evTimerID id_timer1;
    evTimerID id_timer2;
    evFileID id_stdin;
};

static void mytimer(evContext ctx, void *uap, struct timespec due, struct timespec inter)
{
    fprintf(stderr, "timer called (type %d)\n", (int)uap);
    return;
}

static void myselect(evContext ctx, void *uap, int fd, int evmask)
{
    char buf[3];
    struct myenv *myenv = uap;
    int n;

    n = read(fd, buf, 1);
    if (n == 0) {
        evClearTimer(ctx, myenv->id_timer1);
        evClearTimer(ctx, myenv->id_timer2);
        evDeselectFD(ctx, myenv->id_stdin);
        return;
    }
    if (n == 1 && buf[0] == '\n') {
        buf[0] = '\\';
        buf[1] = 'n';
        n = 2;
    }
    buf[n] = '\0';
    fprintf(stderr, "stdin handler called (fd=%d, bytes=%d, char='%s')\n", fd, n, buf);
    return;
}

int main(int argc, char *argv[]) 
{
    evContext ctx;
    evEvent ev;
    struct myenv myenv;
    int rc;

    /* create event context */
    evCreate(&ctx);

    /* register two timers and the stdin select handler */
    evSetTimer(ctx, mytimer, (void *)1, evConsTime(0,0), evConsTime(0,200000000), &myenv.id_timer1);
    evSetTimer(ctx, mytimer, (void *)2, evConsTime(0,0), evConsTime(0,500000000), &myenv.id_timer2);
    evSelectFD(ctx, 0, EV_READ, myselect, &myenv, &myenv.id_stdin);

    /* 
     * main event loop 
     * (can be arbitrary and just has to contain evGetNext/evDispatch!)
     */
    while ((rc = evGetNext(ctx, &ev, EV_WAIT)) == 0)
        if ((rc = evDispatch(ctx, ev)) < 0)
            break;

    /* destroy event context */
    evDestroy(ctx);

    return(0);
}

